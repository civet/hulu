#ifndef LEDHelper_h
#define LEDHelper_h

// set gpio mapping, see: https://github.com/FastLED/FastLED/wiki/ESP8266-notes
//#define FASTLED_ESP8266_RAW_PIN_ORDER

// hide informational message, see: https://github.com/FastLED/FastLED/issues/851
#define FASTLED_INTERNAL

#include <Arduino.h>
#include <FastLED.h>

// Using Multiple Controllers
// https://github.com/FastLED/FastLED/wiki/Multiple-Controller-Examples

class LEDHelper
{
    public:
        LEDHelper();
        LEDHelper& init(struct CRGB *leds, int numLeds);
        LEDHelper& setBlack();
        LEDHelper& setWhite();
        LEDHelper& setRGB(struct CRGB rgb);
        LEDHelper& setRGB(int red, int green, int blue);
        LEDHelper& setHSV(struct CHSV hsv);
        LEDHelper& setHSV(int hue, int saturation, int value);
        LEDHelper& setColor(unsigned long color);
        LEDHelper& setTemperature(int kelvin);
        LEDHelper& setWeights(float *values);
        LEDHelper& setBrightness(int brightness);
        int getBrightness();
        LEDHelper& apply();
        CFastLED* getFastLEDRef();
    private:
        int _numLeds = 0;
        struct CRGB *_leds;
        int _brightness = 255;
};

#endif