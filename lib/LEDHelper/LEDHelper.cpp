#include "LEDHelper.h"
#include "kelvinToRGB.h"

LEDHelper::LEDHelper() 
{
    
}

LEDHelper& LEDHelper::init(struct CRGB *leds, int numLeds)
{
    _leds = leds;
    _numLeds = numLeds;

    return *this;
}

LEDHelper& LEDHelper::setBlack()
{
    for(int i = 0; i < _numLeds; ++i) {
        _leds[i] = CRGB::Black;
    }
    
    return *this;
}

LEDHelper& LEDHelper::setWhite()
{
    for(int i = 0; i < _numLeds; ++i) {
        _leds[i] = CRGB::White;
    }
    
    return *this;
}

LEDHelper& LEDHelper::setRGB(struct CRGB rgb)
{
    for(int i = 0; i < _numLeds; ++i) {
        _leds[i] = rgb;
    }

    return *this;
}

LEDHelper& LEDHelper::setRGB(int red, int green, int blue)
{
    for(int i = 0; i < _numLeds; ++i) {
        _leds[i] = CRGB(red, green, blue);
    }

    return *this;
}

LEDHelper& LEDHelper::setHSV(struct CHSV hsv)
{    
    for(int i = 0; i < _numLeds; ++i) {
        _leds[i] = hsv;
    }

    return *this;
}

LEDHelper& LEDHelper::setHSV(int hue, int saturation, int value)
{    
    for(int i = 0; i < _numLeds; ++i) {
        _leds[i] = CHSV(hue, saturation, value);
    }

    return *this;
}

LEDHelper& LEDHelper::setColor(unsigned long color)
{
    int red = (color >> 16) & 0xFF;
    int green = (color >> 8) & 0xFF;
    int blue = (color >> 0) & 0xFF;

    setRGB(red, green, blue);

    return *this;
}

LEDHelper& LEDHelper::setTemperature(int kelvin)
{   
    unsigned char rgb[3];
    kelvinToRGB(kelvin, rgb);
    FastLED.setTemperature(CRGB(rgb[0], rgb[1], rgb[2]));

    return *this;
}

LEDHelper& LEDHelper::setWeights(float *values)
{
    for(int i = 0; i < _numLeds; ++i) {
        _leds[i] /= (1.0 / values[i]);
    }
    
    return *this;
}

LEDHelper& LEDHelper::setBrightness(int brightness)
{
    _brightness = brightness;

    FastLED.setBrightness(_brightness);

    return *this;
}

int LEDHelper::getBrightness()
{
    return _brightness;
}

LEDHelper& LEDHelper::apply()
{
    FastLED.show();

    return *this;
}

CFastLED* LEDHelper::getFastLEDRef()
{
    return &FastLED;
}