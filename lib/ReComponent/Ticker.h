#ifndef Ticker_h
#define Ticker_h

#include "ReComponent.h"

class Ticker : public ReComponent
{
    public:
        Ticker();
        void start(long interval = 1000);
        void stop();
        void reset();
        void onTick(void (*callback)(int));
        void update(unsigned long deltaTime);
    private:
        long _interval;
        long _remainedTime;
        bool _isRunning;
        int _repeatCount;
        void (*_tickHandler)(int) = 0;
};

#endif