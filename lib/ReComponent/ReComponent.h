#ifndef ReComponent_h
#define ReComponent_h

#include <Arduino.h>

class ReComponent
{
    public:
        virtual void update(unsigned long deltaTime);
};

#endif