#include "ReComponentManager.h"

ReComponentManager::ReComponentManager(MillisFunc fn)
{
    _millis = fn;    
    _list = new LinkedList<ReComponent*>();
    _previousTime = 0;
    _currentTime = 0;
    _deltaTime = 0;
}

ReComponentManager& ReComponentManager::add(ReComponent* component)
{   
    _list->add(component);
    return *this;
}

ReComponentManager& ReComponentManager::remove(int index)
{
    _list->remove(index);
    return *this;
}

ReComponentManager& ReComponentManager::clear()
{
    _list->clear();
    return *this;
}

void ReComponentManager::process()
{
    _currentTime = millis();
    _deltaTime = _currentTime - _previousTime;
    _previousTime = _currentTime;

    ReComponent* component;
    int numComponents = _list->size();
    for(int i = 0; i < numComponents; i++) {
        component = _list->get(i);
        component->update(_deltaTime);
    }    
}