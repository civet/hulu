#include "PushButton.h"

PushButton::PushButton(int pin)
{
    _pin = pin;
    _remainedTime = DELAY_TIME;
}

PushButton& PushButton::setId(int id)
{
    _id = id;
    return *this;
}

PushButton& PushButton::begin() 
{
    _lastState = HIGH; // Keep in mind the default state is HIGH

    // This configuration causes the input to read 
    // HIGH when the switch is open, and LOW when it is closed.
    pinMode(_pin, INPUT_PULLUP);

    return *this;
}

PushButton& PushButton::onPress(void (*callback)(int))
{
    _pressHandler = callback;
    return *this;
}

PushButton& PushButton::onRelease(void (*callback)(int))
{
    _releaseHandler = callback; 
    return *this; 
}

void PushButton::update(unsigned long deltaTime)
{
    // Delay a little bit to avoid bouncing
    //delay(50); 

    _remainedTime -= deltaTime;
    if(_remainedTime > 0) return;
    else _remainedTime = DELAY_TIME;
    
    _state = digitalRead(_pin);
    if(_state != _lastState) {
        if(_state == LOW) {
            if(_pressHandler) _pressHandler(_id);
        }
        else {
            if(_releaseHandler) _releaseHandler(_id);
        }   
    }
    _lastState = _state;
}