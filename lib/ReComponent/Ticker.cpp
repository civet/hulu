#include "Ticker.h"

Ticker::Ticker()
{
   _isRunning = false;
   _repeatCount = 0;
}

void Ticker::start(long interval)
{
    if(_isRunning) return;

    _isRunning = true;    
    _interval = interval;
    _remainedTime = _interval;
}

void Ticker::stop()
{
    if(!_isRunning) return;

    _isRunning = false; 
}

void Ticker::reset()
{
    this->stop();
    _repeatCount = 0;
}

void Ticker::onTick(void (*callback)(int))
{
    _tickHandler = callback;
}

void Ticker::update(unsigned long deltaTime)
{
    if(!_isRunning) return;

    _remainedTime -= deltaTime;
    if(_remainedTime <= 0) {
        _remainedTime = _interval;

        _repeatCount++;

        if(_tickHandler) _tickHandler(_repeatCount);
    }
}