#include "RotaryEncoder.h"

/*
 * The below state table has, for each state (row), the new state
 * to set based on the next encoder output. From left to right in,
 * the table, the encoder outputs are 00, 01, 10, 11, and the value
 * in that position is the new state to set.
 */

#define R_START 0x0

#ifdef HALF_STEP
// Use the half-step state table (emits a code at 00 and 11)
#define R_CCW_BEGIN 0x1
#define R_CW_BEGIN 0x2
#define R_START_M 0x3
#define R_CW_BEGIN_M 0x4
#define R_CCW_BEGIN_M 0x5
const unsigned char stateTable[6][4] = {
    // R_START (00)
    {R_START_M,            R_CW_BEGIN,     R_CCW_BEGIN,  R_START},
    // R_CCW_BEGIN
    {R_START_M | DIR_CCW, R_START,        R_CCW_BEGIN,  R_START},
    // R_CW_BEGIN
    {R_START_M | DIR_CW,  R_CW_BEGIN,     R_START,      R_START},
    // R_START_M (11)
    {R_START_M,            R_CCW_BEGIN_M,  R_CW_BEGIN_M, R_START},
    // R_CW_BEGIN_M
    {R_START_M,            R_START_M,      R_CW_BEGIN_M, R_START | DIR_CW},
    // R_CCW_BEGIN_M
    {R_START_M,            R_CCW_BEGIN_M,  R_START_M,    R_START | DIR_CCW},
};
#else
// Use the full-step state table (emits a code at 00 only)
#define R_CW_FINAL 0x1
#define R_CW_BEGIN 0x2
#define R_CW_NEXT 0x3
#define R_CCW_BEGIN 0x4
#define R_CCW_FINAL 0x5
#define R_CCW_NEXT 0x6

const unsigned char stateTable[7][4] = {
    // R_START
    {R_START,    R_CW_BEGIN,  R_CCW_BEGIN, R_START},
    // R_CW_FINAL
    {R_CW_NEXT,  R_START,     R_CW_FINAL,  R_START | DIR_CW},
    // R_CW_BEGIN
    {R_CW_NEXT,  R_CW_BEGIN,  R_START,     R_START},
    // R_CW_NEXT
    {R_CW_NEXT,  R_CW_BEGIN,  R_CW_FINAL,  R_START},
    // R_CCW_BEGIN
    {R_CCW_NEXT, R_START,     R_CCW_BEGIN, R_START},
    // R_CCW_FINAL
    {R_CCW_NEXT, R_CCW_FINAL, R_START,     R_START | DIR_CCW},
    // R_CCW_NEXT
    {R_CCW_NEXT, R_CCW_FINAL, R_CCW_BEGIN, R_START},
};
#endif

RotaryEncoder::RotaryEncoder(int pinA, int pinB)
{
    _pinA = pinA;
    _pinB = pinB;
}

unsigned char RotaryEncoder::_getDirection() 
{
    // Grab state of input pins.
    unsigned char pinState = (digitalRead(_pinA) << 1) | digitalRead(_pinB);
    
    // Determine new state from the pins and state table.
    _state = stateTable[_state & 0xf][pinState];
    
    // Return emit bits, ie the generated event.
    return _state & 0x30;
}

RotaryEncoder& RotaryEncoder::begin(bool pullup)
{
    if(pullup) {
        // Enable weak pullups
        pinMode(_pinA, INPUT_PULLUP);
        pinMode(_pinB, INPUT_PULLUP);
    }
    else {
        // Set pins to input.
        pinMode(_pinA, INPUT);
        pinMode(_pinB, INPUT);
    }

    return *this;
}

RotaryEncoder& RotaryEncoder::onChange(void (*callback)(unsigned char direction))
{
    _changeHandler = callback;
    return *this;
}

void RotaryEncoder::update(unsigned long deltaTime)
{
    unsigned char dir = _getDirection();
    if(dir != DIR_NONE) {
        if(_changeHandler) _changeHandler(dir);
    }
}
