#ifndef RotaryEncoder_h
#define RotaryEncoder_h

#include "ReComponent.h"

// Enable this to emit codes twice per step.
// #define HALF_STEP

// Values returned by 'getDirection()'
// No complete step yet.
#define DIR_NONE 0x0
// Clockwise step.
#define DIR_CW 0x10
// Counter-clockwise step.
#define DIR_CCW 0x20

class RotaryEncoder : public ReComponent
{
    public:
        RotaryEncoder(int pinA, int pinB);
        RotaryEncoder& begin(bool pullup=true);
        RotaryEncoder& onChange(void (*callback)(unsigned char direction));
        void update(unsigned long deltaTime);
    private:
        int _pinA;
        int _pinB;
        unsigned char _state;
        unsigned char _getDirection();
        void (*_changeHandler)(unsigned char direction) = 0;
};

#endif