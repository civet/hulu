#include "JQ8900_16P.h"

JQ8900_16P::JQ8900_16P() 
{
    
}

bool JQ8900_16P::begin(Stream &stream)
{
    this->begin(stream, false);
}

bool JQ8900_16P::begin(Stream &stream, bool isACK)
{
    _serial = &stream;

    return true;
}

void JQ8900_16P::sendCommand(byte *cmd, int length)
{   
    // #ifdef _DEBUG
    // Serial.print("send:");
    // String output = "";
    // for(int i = 0; i < length; i++) {
    //     output += String(cmd[i], 16);
    //     output += " ";
    // }
    // Serial.println(output);
    // #endif

    _serial->write(cmd, length);
    _serial->flush();
}

void JQ8900_16P::playFrom(int index)
{
    byte valueH = index >> 8 & 0xFF;
    byte valueL = index >> 0 & 0xFF;
    byte checksum = (0xB3 + valueH + valueL) & 0xFF;
    byte cmd[] = {0xAA, 0x07, 0x02, valueH, valueL, checksum};
    sendCommand(cmd, ARRAY_COUNT(cmd));
}

void JQ8900_16P::play(int index)
{
    // stop looping
    if(_isLooping) {
        _isLooping = false;
        sendCommand(COMMAND_MODE_2, 5);
        delay(10);
    }

    if(index > 0) {
        playFrom(index);
    } else {
        sendCommand(COMMAND_PLAY, 4);
    }
}

void JQ8900_16P::stop()
{
    sendCommand(COMMAND_STOP, 4);
    
    // stop looping
    if(_isLooping) {
        _isLooping = false;
        delay(10);
        sendCommand(COMMAND_MODE_2, 5);
    }
}

void JQ8900_16P::setMode(int mode)
{
    _mode = mode;

    byte value = mode & 0xFF;
    byte checksum = (0xC3 + mode) & 0xFF;
    byte cmd[] = {0xAA, 0x18, 0x01, value, checksum};
    sendCommand(cmd, ARRAY_COUNT(cmd));
}

void JQ8900_16P::setVolume(int volume)
{
    _volume = volume;

    byte value = volume & 0xFF;
    byte checksum = (0xBE + volume) & 0xFF;
    byte cmd[] = {0xAA, 0x13, 0x01, value, checksum};
    sendCommand(cmd, ARRAY_COUNT(cmd));
}

void JQ8900_16P::volume(int volume)
{
    this->setVolume(volume);
}

int JQ8900_16P::getMode()
{
    return _mode;
}

int JQ8900_16P::getVolume()
{
    return _volume;
}

void JQ8900_16P::loop(int index)
{
    // start looping
    _isLooping = true;
    sendCommand(COMMAND_MODE_1, 5);

    playFrom(index);
}
