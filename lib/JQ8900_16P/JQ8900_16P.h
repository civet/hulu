#ifndef JQ8900_16P_h
#define JQ8900_16P_h

#include <Arduino.h>
#include <SoftwareSerial.h>

#define ARRAY_COUNT(a) (sizeof(a)/sizeof((a)[0]))

class JQ8900_16P
{
    public:
        JQ8900_16P();
        bool begin(Stream &stream);
        bool begin(Stream &stream, bool isACK);
        void sendCommand(byte *cmd, int length);
        void playFrom(int index);
        void play(int index=0);
        void stop();
        void loop(int index);
        void setMode(int mode);
        void setVolume(int volume);
        int getMode();
        int getVolume();
        void volume(int volume);
    private:
        Stream* _serial;
        int _volume = 20;
        int _mode = 2;
        bool _isLooping = true;
        byte COMMAND_PLAY[4] = {0xAA, 0x02, 0x00, 0xAC};
        byte COMMAND_PAUSE[4] = {0xAA, 0x03, 0x00, 0xAD};
        byte COMMAND_STOP[4] = {0xAA, 0x04, 0x00, 0xAE};
        byte COMMAND_PREV[4] = {0xAA, 0x05, 0x00, 0xAF};
        byte COMMAND_NEXT[4] = {0xAA, 0x06, 0x00, 0xB0};
        byte COMMAND_ABORT[4] = {0xAA, 0x10, 0x00, 0xBA};
        byte COMMAND_MODE_0[5] = {0xAA, 0x18, 0x01, 0x00, 0xC3};
        byte COMMAND_MODE_1[5] = {0xAA, 0x18, 0x01, 0x01, 0xC4};
        byte COMMAND_MODE_2[5] = {0xAA, 0x18, 0x01, 0x02, 0xC5};
};

#endif