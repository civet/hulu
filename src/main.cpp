#define __USE_JQ8900_16P 0 // 0: DFPlayerMini | 1: JQ8900-16P

#include <Arduino.h>
#include <SoftwareSerial.h>
#include "ReComponentManager.h"
#include "PushButton.h"
#include "Ticker.h"
#include "LEDHelper.h"
#include "PeakValues.h"

#if __USE_JQ8900_16P
#include "JQ8900_16P.h"
#else
#include "DFRobotDFPlayerMini.h"
#endif

// Pins
#define SOFT_SERIAL_RX 10
#define SOFT_SERIAL_TX 11
#define BUTTON_PIN 2
#define LEDS_PIN_1 4
#define LEDS_PIN_2 5
#define NUM_LEDS_1 8
#define NUM_LEDS_2 8

// MP3 Player
SoftwareSerial softSerial(SOFT_SERIAL_RX, SOFT_SERIAL_TX);

#if __USE_JQ8900_16P
JQ8900_16P player;
#else
DFRobotDFPlayerMini player;
#endif

bool useACK = false;
int defaultVolume = 30; // default volume [0 ~ 30]

// LED Strip
LEDHelper ledHelper;
CRGB leds[NUM_LEDS_1 + NUM_LEDS_2];
float ledWeights[NUM_LEDS_1 + NUM_LEDS_2] = {
    1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
    1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
};
int ledGloablBrightness = 32; // global brightness [0 ~ 255]

CHSV colorBlue = {157, 250, 250};
CHSV colorRed = {0, 214, 250};

int stepBlue = 2;
unsigned long delayRed = 500; // millisecond

// Component Manager
ReComponentManager componentManager(millis);

// Button
PushButton button(BUTTON_PIN);
int buttonState = 0;

// Timer
Ticker faderBlue;
Ticker faderRed;

unsigned long t = 0;
int idx = 0;

// Handlers

void handleButtonPress(int id)
{
    // do nothing
}

void handleButtonRelease(int id)
{
    buttonState++;
    if(buttonState > 3) buttonState = 1;

    switch(buttonState) {
        case 1:
            ledHelper.setHSV(colorBlue.h, colorBlue.s, 0)
                    .setWeights(ledWeights)
                    .apply();
                    
            faderBlue.start(16);
            break;

        case 2:
            faderBlue.reset();
            //ledHelper.setBlack().apply();
            
            ledHelper.setHSV(colorRed.h, colorRed.s, 0)
                .setWeights(ledWeights)
                .apply();

            t = millis();
            idx = 0;
            faderRed.start(16);

            // play sfx
            player.loop(1);
            break;

        case 3:
            faderRed.reset();
            ledHelper.setBlack().apply();

            // stop sfx
            player.stop();
            break;

        default:
            break;
    }
}

void handleFadeBlue(int repeatCount) 
{
    ledHelper.setHSV(colorBlue.h, colorBlue.s, repeatCount * stepBlue)
        .setWeights(ledWeights)
        .apply();

    if(repeatCount * stepBlue >= colorBlue.v) faderBlue.reset();
}

void handleFadeRed(int repeatCount) 
{
    if(t > 0 && millis() - t < delayRed) return;
    t = 0;

    idx += 2;
    if(idx >= PEAK_VALUES_LENGTH) idx = 0;

    float leftPeak = pgm_read_float_near(PEAK_VALUES + idx);
    float rightPeak = pgm_read_float_near(PEAK_VALUES + idx + 1);
    int value = colorRed.v * (leftPeak + rightPeak) * 2;
    
    ledHelper.setHSV(colorRed.h, colorRed.s, value)
        .setWeights(ledWeights)
        .apply();
}


void setup() 
{
    Serial.begin(115200);

    // FastLED.addLeds<型号, DIN, 三原色次序>(leds, 起始索引, LED数量);
    FastLED.addLeds<WS2812B, LEDS_PIN_1, GRB>(leds, 0, NUM_LEDS_1);
    FastLED.addLeds<WS2812B, LEDS_PIN_2, GRB>(leds, NUM_LEDS_1, NUM_LEDS_2);
    //FastLED.addLeds<WS2812B, LEDS_PIN_3, RGB>(leds, NUM_LEDS_1 + NUM_LEDS_2, NUM_LEDS_3);

    ledHelper.init(leds, NUM_LEDS_1 + NUM_LEDS_2)
        .setBrightness(ledGloablBrightness)
        .setBlack()
        .apply();
    
    button.begin()
        .onPress(handleButtonPress)
        .onRelease(handleButtonRelease);

    faderBlue.onTick(handleFadeBlue);
    faderRed.onTick(handleFadeRed);

    componentManager
        .add(&button)
        .add(&faderBlue)
        .add(&faderRed)
        ;
    
    softSerial.begin(9600);

    if (!player.begin(softSerial, useACK)) {
        Serial.println(F("Unable to connect MP3Player:"));
        Serial.println(F("1.Please recheck the connection!"));
        Serial.println(F("2.Please insert the SD card!"));
    } else {
        Serial.println(F("MP3Player init."));
        player.volume(defaultVolume);
    }
}

void loop() 
{
    componentManager.process();
}
